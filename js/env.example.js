// un archivo de configuracion como este debe ser incluido en la carpeta /js con el 
// nombre env.js para poder hacer funcionar el proyecto (solo para el front-end)
(function (window) {
  window.__env = window.__env || {};
  window.__env.apiUrl = 'http://148.202.151.201/secciones/public';
  window.__env.baseUrl = '/preregistro';
  window.__env.enableDebug = true;
  window.__env.whiteListedDomains = ['148.202.151.201'];
}(this));
