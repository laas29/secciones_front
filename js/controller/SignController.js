(function() {
    'use strict';
    angular.module('secciones_front').
    controller('SignController', ['localStorageService', 'ACLService', 'jwtHelper', SignController]);

    function SignController(localStorageService, ACLService, jwtHelper) {

        var token = localStorageService.get('jwt_token');

        if( token && !jwtHelper.isTokenExpired(token)){
            ACLService.goToDefaultSection();
        }
        else{
            location.hash = '#/home';
        }

    }

})();
