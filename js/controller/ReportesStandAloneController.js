(function() {
    'use strict';
    angular.module('secciones_front').
    controller('ReportesStandAloneController', ['$stateParams', '$http', '$scope', 'PNotify', '$state', 'NgTableParams', '$filter', 'localStorageService', '$window', ReportesStandAloneController]);

    function ReportesStandAloneController($stateParams, $http, $scope, PNotify, $state, ngTableParams, $filter, localStorageService, $window) {

        var reporteAlumnosSolicitantes = [],
            that = this;
        var request = {
            method: 'GET',
            url: __env.apiUrl + '/reportes/ciclo/' + $stateParams.idCiclo + '/materia/' + $stateParams.clave + '/programas/' + $stateParams.pathPrograms,
            //data: {carreras: that.idsCarrerasElegidas, id_ciclo: that.ciclo.id_ciclo}
        };

        that.situaciones = [
            { id: "", title: "Sin filtro" },
            { id: "Primera vez", title: "Primera vez" },
            { id: "Repetidor", title: "Repetidor" },
            { id: "Art 33", title: "Art 33" },
            { id: "Art 34", title: "Art 34" },
            { id: "Art 35", title: "Art 35" }
        ];

        that.tiposHorarios = [
            { id: "", title: "Sin filtro" },
            { id: "Matutino", title: "Matutino" },
            { id: "Vespertino", title: "Vespertino" },
            { id: "Sabatino", title: "Sabatino" },
            { id: "Cualquiera", title: "Cualquiera" }
        ];

        if ($stateParams.id_centro) {

            request = {
                method: 'GET',
                url: __env.apiUrl + '/reportes/movilidad/ciclo/' + $stateParams.idCiclo + '/materia/' +  $stateParams.clave + '/centro/' + $stateParams.id_centro + '/programas/' + $stateParams.pathPrograms
            };

        }

        $http(request).then(function successCallback(response) {

            reporteAlumnosSolicitantes = response.data.result;

            that.titulo = $stateParams.clave;

            if($stateParams.id_centro){

                that.titulo = response.data.result[0].campus +' '+ $stateParams.clave;

            }

            that.ngTableAlumnosSolicitantes.reload();

        }, function errorCallback(response) {
            PNotify.notify({
                title: 'Error.',
                text: 'Hubo un error al recuperar los alumnos solicitantes.',
                type: 'error'
            });
        });

        that.ngTableAlumnosSolicitantes = new ngTableParams({
            page: 1,
            count: 10
        }, {
            counts: [10, 50, 100, 300, 500, 1000],
            filterDelay: 0,
            getData: function(params) {

                var inputs = params.filter();
                var sorting = params.sorting();

                var alumnosSolicitantes = reporteAlumnosSolicitantes;

                alumnosSolicitantes = params.sorting() ? $filter('orderBy')(alumnosSolicitantes, params.orderBy()) : alumnosSolicitantes;
                alumnosSolicitantes = params.filter() ? $filter('filter')(alumnosSolicitantes, params.filter()) : alumnosSolicitantes;
                params.total(alumnosSolicitantes.length);
                alumnosSolicitantes = alumnosSolicitantes.slice((params.page() - 1) * params.count(), params.page() * params.count());

                return alumnosSolicitantes;

            }
        });

    }

})();
