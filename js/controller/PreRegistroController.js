(function() {
    'use strict';
    angular.module('secciones_front').
    controller('PreRegistroController', ['$http', '$scope', 'PNotify', '$state', 'NgTableParams', '$filter', 'ACLService', PreRegistroController]);

    function PreRegistroController($http, $scope, PNotify, $state, ngTableParams, $filter, ACLService) {

        var that = this, materiasOtraCarrera = [], idMateriasSeleccionadas = [];
        that.inputs = {};
        that.materiasSeleccionadas = [];
        that.totalCreditos =  0;
        that.programas = [];
        that.codePattern = "^\\w{5,7}$";
        that.todosLosCampus = null;
        that.claveMovilidad = null;
        that.materiasMovilidad = [];
        that.clavesMateriasMovilidad = [];
        that.situaciones = ["Primera vez","Repetidor","Art 33","Art 34","Art 35"];
        that.horarios = ["Matutino","Vespertino","Sabatino","Cualquiera"];
        that.sessionValues = ACLService.getSessionValues();
        
        that.print = function(){
            window.print();
        };

        that.seleccionaMateria = function(materia){

            if(_.indexOf(idMateriasSeleccionadas, materia.id_materia)==-1){

                that.totalCreditos += parseInt(materia.creditos, 10);
                materia.situacion = "Primera vez";
                materia.horario = "Matutino";
                that.materiasSeleccionadas.push(materia);
                idMateriasSeleccionadas.push(materia.id_materia);
                $scope.dc.preregistroModificado = true;
            }
        };

        that.seleccionaMateriaMovilidad = function(){

            var claveMovilidad = that.claveMovilidad.toUpperCase();

            if(_.indexOf(that.clavesMateriasMovilidad, that.claveMovilidad)==-1){
                that.clavesMateriasMovilidad.push(claveMovilidad);
                that.materiasMovilidad.push({'clave': claveMovilidad, 'campus': that.campusMovilidad});
            }

            that.claveMovilidad = "";
            that.campusMovilidad = null;
            $scope.dc.preregistroModificado = true;

        };

        that.eliminarElementoMovilidad = function(index){

            that.materiasMovilidad.splice(index, 1);
            that.clavesMateriasMovilidad.splice(index, 1);
            $scope.dc.preregistroModificado = true;

        };

        that.borrarTodo = function(){

            that.totalCreditos = 0;
            that.materiasSeleccionadas = [];
            idMateriasSeleccionadas = [];
            that.materiasMovilidad = [];
            that.clavesMateriasMovilidad = [];
            $scope.dc.preregistroModificado = true;

        };

        that.eliminaElemento = function(index){

            var matAElim = that.materiasSeleccionadas[index];
            that.totalCreditos -= matAElim.creditos;
            idMateriasSeleccionadas.splice(index, 1);
            that.materiasSeleccionadas.splice(index, 1);
            $scope.dc.preregistroModificado = true;

        }

        that.cambiarCarrera = function($item, $model, $label, $event){

            $http({
                method: 'GET',
                url: __env.apiUrl + '/programa/' + $item.id_programa + '/materias'
            }).then(function successCallback(response) {

                materiasOtraCarrera = response.data.result;
                that.ngTableMateriasOtraCarrera.reload();

            }, function errorCallback(response) {
                PNotify.notify({
                    title: 'Error.',
                    text: 'Hubo un error al recuperar los listados de las carreras.',
                    type: 'error'
                });
            });

        }

        that.guardarPreregistro = function(){

            var clavesMovilidad = _.map(that.materiasMovilidad, function(el){

                return {"clave": el.clave, "campus": el.campus.id_centro};

            });

            var materias = angular.copy(that.materiasSeleccionadas);

            var materias = _.map(materias, function(el){

                delete el.nombre;
                delete el.creditos;
                delete el.clave;
                return el;

            });

            $http({
                method: 'POST',
                url: __env.apiUrl + '/preregistro',
                data: {
                    'materias': materias,
                    'movilidad': clavesMovilidad,
                }
            }).then(function successCallback(response) {

                PNotify.notify({
                    text: 'Se ha guardado el pre-registro.',
                    title: 'Pre-registro guardado.',
                    type: 'success'
                });

                $scope.dc.preregistroModificado = false;

            }, function errorCallback(response) {
                PNotify.notify({
                    text: 'Hubo un error al guardar el pre-registro, intentelo más tarde.',
                    title: 'Error.',
                    type: 'error'
                });

                $scope.dc.preregistroModificado = false;
            });

        };

        $http({
            method: 'GET',
            url: __env.apiUrl + '/preregistro/materias/alumno/'+that.sessionValues.idAlumno
        }).then(function successCallback(response) {

            var materias = response.data.result.materias;
            var movilidad = response.data.result.movilidad;

            console.log(materias);

            that.materiasSeleccionadas = materias;

            _.map(materias, function(el){

                idMateriasSeleccionadas.push(el.id_materia);
                that.totalCreditos += parseInt(el.creditos);

            });

            _.map(movilidad, function(el){

                that.clavesMateriasMovilidad.push(el.clave);
                that.materiasMovilidad.push({'clave': el.clave, 'campus': {'nombre':el.nombre, 'id_centro': el.id_centro}});

            });

        }, function errorCallback(response) {
            PNotify.notify({
                title: 'Error.',
                text: 'Hubo un error al recuperar el listado de materias elegidas.\n\n'+response.data.debugInfo,
                type: 'error'
            });
        });

        $http({
            method: 'GET',
            url: __env.apiUrl + '/public/listados/campus'
        }).then(function successCallback(response) {

            that.todosLosCampus = response.data.result;

        }, function errorCallback(response) {
            PNotify.notify({
                title: 'Error.',
                text: 'Hubo un error al recuperar el listado de campus.',
                type: 'error'
            });
        });

        $http({
            method: 'GET',
            url: __env.apiUrl + '/public/listados/carreras'
        }).then(function successCallback(response) {

            that.programas = response.data.result;

        }, function errorCallback(response) {
            PNotify.notify({
                title: 'Error.',
                text: 'Hubo un error al recuperar los listados de las carreras.',
                type: 'error'
            });
        });

        that.ngTableMateriasOtraCarrera = new ngTableParams({
                    page: 1,
                    count: 10
                }, {
                    filterDelay: 0,
                    getData: function(params) {

                        var inputs = params.filter();
                        var sorting = params.sorting();

                        var materiasFiltradas = materiasOtraCarrera;

                        materiasFiltradas = params.sorting() ? $filter('orderBy')(materiasFiltradas, params.orderBy()) : materiasFiltradas;
                        materiasFiltradas = params.filter() ? $filter('filter')(materiasFiltradas, params.filter()) : materiasFiltradas;
                        params.total(materiasFiltradas.length);
                        materiasFiltradas = materiasFiltradas.slice((params.page() - 1) * params.count(), params.page() * params.count());

                        return materiasFiltradas;

                    }
                });

        $http({
            method: 'GET',
            url: __env.apiUrl + '/programa/' + that.sessionValues.carrera + '/materias'
        }).then(function successCallback(response) {

                var materias = response.data.result;

                that.ngTableMateriasCarrera = new ngTableParams({
                    page: 1,
                    count: 10
                }, {
                    filterDelay: 0,
                    getData: function(params) {

                        var inputs = params.filter();
                        var sorting = params.sorting();

                        var materiasFiltradas = materias;

                        materiasFiltradas = params.sorting() ? $filter('orderBy')(materiasFiltradas, params.orderBy()) : materiasFiltradas;
                        materiasFiltradas = params.filter() ? $filter('filter')(materiasFiltradas, params.filter()) : materiasFiltradas;
                        params.total(materiasFiltradas.length);
                        materiasFiltradas = materiasFiltradas.slice((params.page() - 1) * params.count(), params.page() * params.count());

                        return materiasFiltradas;

                    }
                });
            },
            function errorCallback(response) {
                PNotify.notify({
                    title: 'Error.',
                    text: 'Hubo un error al recuperar los listados de los materias escolares.',
                    type: 'error'
                });

            });

    }

})();
