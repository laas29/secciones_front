(function() {
    'use strict';
    angular.module('secciones_front').
    controller('EdicionRecursoController', ['$stateParams', '$http', '$scope', 'PNotify', '$state', 'NgTableParams', '$filter', 'localStorageService', '$window', '$q', EdicionRecursoController]);

    function EdicionRecursoController($stateParams, $http, $scope, PNotify, $state, ngTableParams, $filter, localStorageService, $window, $q) {

        var reporteAlumnosSolicitantes = [],
            that = this,
            windowCounter = 0;
        var urlParams = $stateParams.pathPrograms.split("/");
        var idRecurso = urlParams[1];
        that.inhabilitarGuardado = false;

        that.__formateaModelo = function() {

            var nuevoModelo = _.clone(that.modelo);
            delete nuevoModelo.idsElegidos;
            delete nuevoModelo.idsRemovidos;
            return nuevoModelo;
        };

        that.modelo = {};
        that.modelo.idsElegidos = [];
        that.modelo.idsRemovidos = [];

        that.titulo = idRecurso?"Editar recurso":"Crear nuevo recurso";

        that.colecciones = {
            alumnos: {
                model: [],
                requests: {
                    fetch: {
                        method: 'GET',
                        url: __env.apiUrl + '/recurso/alumno/' + idRecurso
                    },
                    create: {
                        method: 'POST',
                        url: __env.apiUrl + '/recurso/alumno',
                        //data: {carreras: that.idsCarrerasElegidas, id_ciclo: that.ciclo.id_ciclo}
                    },
                    delete: {
                        method: 'DELETE',
                        url: __env.apiUrl + '/reportes/ciclo/' + $stateParams.idCiclo + '/materia/' + $stateParams.clave + '/programas/' + $stateParams.pathPrograms,
                        //data: {carreras: that.idsCarrerasElegidas, id_ciclo: that.ciclo.id_ciclo}
                    },
                    update: {
                        method: 'PUT',
                        url: __env.apiUrl + '/recurso/alumno/' + idRecurso
                    }
                }
            },
            programas: {
                tabla: null,
                model: [],
                csv: {},
                requests: {
                    fetch: {
                        method: 'GET',
                        url: __env.apiUrl + '/recurso/programa/' + idRecurso,
                        data: { skip: 0, limit: 10 }
                        //data: {carreras: that.idsCarrerasElegidas, id_ciclo: that.ciclo.id_ciclo}
                    },
                    create: {
                        method: 'POST',
                        url: __env.apiUrl + '/recurso/programa'
                    },
                    delete: {
                        method: 'GET',
                        url: __env.apiUrl + '/reportes/ciclo/' + $stateParams.idCiclo + '/materia/' + $stateParams.clave + '/programas/' + $stateParams.pathPrograms,
                        //data: {carreras: that.idsCarrerasElegidas, id_ciclo: that.ciclo.id_ciclo}
                    },
                    update: {
                        method: 'PUT',
                        url: __env.apiUrl + '/recurso/programa/' + idRecurso
                    }
                }
            },
            materias: {
                tabla: null,
                model: [],
                csv: {},
                requests: {
                    fetch: {
                        method: 'GET',
                        url: __env.apiUrl + '/recurso/materia/' + idRecurso,
                        data: { skip: 0, limit: 10 }
                        //data: {carreras: that.idsCarrerasElegidas, id_ciclo: that.ciclo.id_ciclo}
                    },
                    create: {
                        method: 'POST',
                        url: __env.apiUrl + '/recurso/materia'
                    },
                    delete: {
                        method: 'GET',
                        url: __env.apiUrl + '/reportes/ciclo/' + $stateParams.idCiclo + '/materia/' + $stateParams.clave + '/programas/' + $stateParams.pathPrograms,
                        //data: {carreras: that.idsCarrerasElegidas, id_ciclo: that.ciclo.id_ciclo}
                    },
                    update: {
                        method: 'PUT',
                        url: __env.apiUrl + '/recurso/materia/' + idRecurso
                    }
                }
            },
            ciclos: {
                tabla: null,
                model: [],
                csv: {},
                requests: {
                    fetch: {
                        method: 'GET',
                        url: __env.apiUrl + '/recurso/ciclo/' + idRecurso,
                        data: { skip: 0, limit: 10 }
                        //data: {carreras: that.idsCarrerasElegidas, id_ciclo: that.ciclo.id_ciclo}
                    },
                    create: {
                        method: 'POST',
                        url: __env.apiUrl + '/recurso/ciclo'
                    },
                    delete: {
                        method: 'GET',
                        url: __env.apiUrl + '/reportes/ciclo/' + $stateParams.idCiclo + '/materia/' + $stateParams.clave + '/programas/' + $stateParams.pathPrograms,
                        //data: {carreras: that.idsCarrerasElegidas, id_ciclo: that.ciclo.id_ciclo}
                    },
                    update: {
                        method: 'PUT',
                        url: __env.apiUrl + '/recurso/ciclo/' + idRecurso
                    }
                }
            },
            centros: {
                tabla: null,
                model: [],
                csv: {},
                requests: {
                    fetch: {
                        method: 'GET',
                        url: __env.apiUrl + '/recurso/centro/' + idRecurso,
                        data: { skip: 0, limit: 10 }
                    },
                    create: {
                        method: 'POST',
                        url: __env.apiUrl + '/recurso/centro',
                        //data: {carreras: that.idsCarrerasElegidas, id_ciclo: that.ciclo.id_ciclo}
                    },
                    delete: {
                        method: 'DELETE',
                        url: __env.apiUrl + '/recurso/centro/' + idRecurso
                    },
                    update: {
                        method: 'PUT',
                        url: __env.apiUrl + '/recurso/centro/' + idRecurso
                    }
                }
            },
            departamentos: {
                tabla: null,
                model: [],
                csv: {},
                requests: {
                    fetch: {
                        method: 'GET',
                        url: __env.apiUrl + '/recurso/departamento/' + idRecurso,
                        data: { skip: 0, limit: 10 }
                        //data: {carreras: that.idsCarrerasElegidas, id_ciclo: that.ciclo.id_ciclo}
                    },
                    create: {
                        method: 'POST',
                        url: __env.apiUrl + '/recurso/departamento'
                    },
                    delete: {
                        method: 'GET',
                        url: __env.apiUrl + '/reportes/ciclo/' + $stateParams.idCiclo + '/materia/' + $stateParams.clave + '/programas/' + $stateParams.pathPrograms,
                        //data: {carreras: that.idsCarrerasElegidas, id_ciclo: that.ciclo.id_ciclo}
                    },
                    update: {
                        method: 'PUT',
                        url: __env.apiUrl + '/recurso/departamento/' + idRecurso
                    }
                }
            },
            usuarios: {
                tabla: null,
                model: [],
                csv: {},
                requests: {
                    fetch: {
                        method: 'GET',
                        url: __env.apiUrl + '/recurso/usuario/' + idRecurso,
                        data: { skip: 0, limit: 10 }
                        //data: {carreras: that.idsCarrerasElegidas, id_ciclo: that.ciclo.id_ciclo}
                    },
                    create: {
                        method: 'POST',
                        url: __env.apiUrl + '/recurso/usuario'
                        //data: {carreras: that.idsCarrerasElegidas, id_ciclo: that.ciclo.id_ciclo}
                    },
                    delete: {
                        method: 'GET',
                        url: __env.apiUrl + '/reportes/ciclo/' + $stateParams.idCiclo + '/materia/' + $stateParams.clave + '/programas/' + $stateParams.pathPrograms,
                        //data: {carreras: that.idsCarrerasElegidas, id_ciclo: that.ciclo.id_ciclo}
                    },
                    update: {
                        method: 'PUT',
                        url: __env.apiUrl + '/recurso/usuario/' + idRecurso
                    }
                }
            }
        };
        that.nombreColeccion = urlParams[0];
        that.coleccion = that.colecciones[that.nombreColeccion];

        that.buscaElemento = function(id, id_name, coleccion) {

            var query = {};
            query[id_name] = id;

            return coleccion[_.findIndex(coleccion, query)];

        };

        function getMethods(colectionName) {

            if(colectionName == 'ciclos'){

                that.__formateaModelo = function() {

                    var nuevoModelo = _.clone(that.modelo);
                    delete nuevoModelo.idsElegidos;
                    delete nuevoModelo.idsRemovidos;
                    nuevoModelo.preregistro_activo = nuevoModelo.preregistro_activo || 0;
                    return nuevoModelo;

                };

            }
            else if (colectionName == 'programas') {

                that.cargaMaterias = function(nombreMateria) {

                    return $http({
                        method: 'POST',
                        url: __env.apiUrl + '/reportes/materias',
                        data: { "page": 1, "count": 10, "filter": { "nombre": nombreMateria }, "sorting": {}, "group": {} }
                    }).then(function successCallback(response) {

                        return response.data.result.rows;

                    }, function errorCallback(response) {
                        PNotify.notify({
                            title: 'Error.',
                            text: 'Hubo un error al recuperar los listados de las carreras.',
                            type: 'error'
                        });
                    });

                };

                that.__formateaModelo = function() {

                    var nuevoModelo = _.clone(that.modelo);
                    delete nuevoModelo.elementosElegidos;
                    delete nuevoModelo.elementoElegido;
                    nuevoModelo.idsElegidos = _.difference(that.modelo.idsElegidos, that.modelo.idsRemovidos);
                    nuevoModelo.idsRemovidos = _.difference(that.modelo.idsRemovidos, that.modelo.idsElegidos);
                    if (!nuevoModelo.idsElegidos.length) {
                        delete nuevoModelo.idsElegidos;
                    }
                    if (!nuevoModelo.idsRemovidos.length) {
                        delete nuevoModelo.idsRemovidos;
                    }
                    nuevoModelo.preregistro_activo = nuevoModelo.preregistro_activo || 0;
                    nuevoModelo.id_departamento = that.departamentoPrograma.id_departamento;
                    return nuevoModelo;

                };

                that.__onRemoveChip = function(data) {

                };

                that.claveProgramaInicial = that.modelo.clave;

                that.revisaExistenciaPrograma = function() {

                    console.log("Revisando existencia programa");

                    if (that.modelo.clave != that.claveProgramaInicial) {

                        $http({
                            method: 'GET',
                            url: __env.apiUrl + '/public/consulta/programa/' + that.modelo.clave
                        }).then(function successCallback(response) {

                            if (response.data.result.length) {
                                // $scope.formRegistro.clave.registrado = true;
                                $scope.formRegistro.clave.$setValidity('codigoUnico', false);

                            } else {
                                // $scope.formRegistro.clave.registrado = false;
                                $scope.formRegistro.clave.$setValidity('codigoUnico', true);
                            }

                        }, function errorCallback(response) {
                            PNotify.notify({
                                title: 'Error.',
                                text: 'Hubo un error al buscar la clave del programa.',
                                type: 'error'
                            });
                        });

                    } else {
                        $scope.formRegistro.clave.$setValidity('codigoUnico', true);
                    }

                };


            } else if (colectionName == 'alumnos') {

                that.codigoAlumnoInicial = that.modelo.codigo;
                that.codePattern = "^\\d{9,11}\\w?$";

                that.__formateaModelo = function() {

                    var nuevoModelo = _.clone(that.modelo);
                    delete nuevoModelo.idsElegidos;
                    delete nuevoModelo.idsRemovidos;
                    delete nuevoModelo.role;
                    nuevoModelo.id_programa = that.programaAlumno.id_programa;
                    nuevoModelo.id_generacion = that.generacionAlumno.id_ciclo;
                    return nuevoModelo;

                };

                that.revisaExistenciaAlumno = function() {

                    console.log("Revisando existencia alumno");

                    if ($scope.formRegistro.codigo.$validators.pattern()) {

                        if (that.modelo.codigo != that.codigoAlumnoInicial) {

                            $http({
                                method: 'GET',
                                url: __env.apiUrl + '/public/consulta/alumno/' + that.modelo.codigo
                            }).then(function successCallback(response) {

                                if (response.data.result.length) {
                                    console.log("El alumno ya existe");
                                    // $scope.formRegistro.codigo.registrado = true;
                                    $scope.formRegistro.codigo.$setValidity('codigoUnico', false);

                                } else {
                                    console.log("El alumno no existe");
                                    // $scope.formRegistro.codigo.registrado = false;
                                    $scope.formRegistro.codigo.$setValidity('codigoUnico', true);
                                }


                            }, function errorCallback(response) {
                                PNotify.notify({
                                    title: 'Error.',
                                    text: 'Hubo un error al buscar el código de estudiante.',
                                    type: 'error'
                                });
                            });

                        } else {
                            $scope.formRegistro.codigo.$setValidity('codigoUnico', true);
                        }

                    } else {
                        console.log("El codigo aun no es válido.");
                    }

                }

            } else if (colectionName == 'departamentos') {

                that.__formateaModelo = function() {

                    var nuevoModelo = _.clone(that.modelo);
                    delete nuevoModelo.idsElegidos;
                    delete nuevoModelo.idsRemovidos;
                    nuevoModelo.id_division = that.divisionDepartamento.id_division;
                    return nuevoModelo;

                };

            } else if (colectionName == 'materias') {

                that.claveProgramaInicial = that.modelo.clave;

                that.revisaExistenciaMateria = function() {

                    console.log("Revisando existencia materia");

                    if (that.modelo.clave != that.claveProgramaInicial) {

                        $http({
                            method: 'GET',
                            url: __env.apiUrl + '/public/consulta/materia/' + that.modelo.clave
                        }).then(function successCallback(response) {

                            if (response.data.result.length) {
                                // $scope.formRegistro.clave.registrado = true;
                                $scope.formRegistro.clave.$setValidity('codigoUnico', false);

                            } else {
                                // $scope.formRegistro.clave.registrado = false;
                                $scope.formRegistro.clave.$setValidity('codigoUnico', true);
                            }

                        }, function errorCallback(response) {
                            PNotify.notify({
                                title: 'Error.',
                                text: 'Hubo un error al buscar la clave de la materia.',
                                type: 'error'
                            });
                        });

                    } else {
                        $scope.formRegistro.clave.$setValidity('codigoUnico', true);
                    }


                };

                that.__formateaModelo = function() {

                    var nuevoModelo = _.clone(that.modelo);
                    delete nuevoModelo.elementoElegido;
                    delete nuevoModelo.elementosElegidos;
                    nuevoModelo.idsElegidos = _.difference(that.modelo.idsElegidos, that.modelo.idsRemovidos);
                    nuevoModelo.idsRemovidos = _.difference(that.modelo.idsRemovidos, that.modelo.idsElegidos);
                    if (!nuevoModelo.idsElegidos.length) {
                        delete nuevoModelo.idsElegidos;
                    }
                    if (!nuevoModelo.idsRemovidos.length) {
                        delete nuevoModelo.idsRemovidos;
                    }
                    return nuevoModelo;

                };

            } else if (colectionName == 'usuarios') {

                that.__formateaModelo = function() {

                    var nuevoModelo = _.clone(that.modelo);
                    delete nuevoModelo.idsElegidos;
                    delete nuevoModelo.idsRemovidos;
                    nuevoModelo.role = nuevoModelo.role || 'user';
                    return nuevoModelo;

                };

                that.mandarCorreo = function() {

                    PNotify.confirm({
                        title: 'Confirma la acción.',
                        text: '¿Estas seguro de querer reseter la contraseña?',
                        yesCallback: function() {

                            var reqConf = {
                                method: 'POST',
                                url: __env.apiUrl + '/usuario/recuperar-password',
                                data: { "email": that.modelo.email }
                            };

                            $http(reqConf).then(function successCallback(response) {

                                PNotify.notify({
                                    title: 'Recurso modificado.',
                                    text: 'Se ha enviado al correo del usuario una nueva contraseña.',
                                    type: 'success'
                                });


                            }, function errorCallback(response) {

                                var debugText = _.reduce(response.data.debugData, function(memo, dd) {

                                    return memo + dd + '\n\n';

                                }, "\n" + response.data.debugInfo + "\n\n");

                                PNotify.notify({
                                    title: 'Error.',
                                    text: 'Hubo un error al modificar el recurso.' + debugText,
                                    type: 'error'
                                });
                            });
                        },
                        noCallback: function() {

                        }
                    });

                };
            }

        }

        function getPromises(colectionName) {

            var promises = [];

            if (colectionName == 'alumnos') {

                return [$http({
                        method: 'GET',
                        url: __env.apiUrl + '/public/listados/carreras'
                    }).then(function successCallback(response) {

                        console.log("respuesta ", response);
                        that.programas = response.data.result;
                        that.programaAlumno = that.buscaElemento(that.modelo.id_programa, 'id_programa', that.programas);

                    }, function errorCallback(response) {
                        PNotify.notify({
                            title: 'Error.',
                            text: 'Hubo un error al recuperar los listados de las carreras.',
                            type: 'error'
                        });
                    }),
                    $http({
                        method: 'GET',
                        url: __env.apiUrl + '/public/listados/ciclos'
                    }).then(function successCallback(response) {

                        console.log("ciclos ", response);
                        that.ciclos = response.data.result;
                        that.generacionAlumno = that.buscaElemento(that.modelo.id_generacion, 'id_ciclo', that.ciclos);

                    }, function errorCallback(response) {
                        PNotify.notify({
                            title: 'Error.',
                            text: 'Hubo un error al recuperar los listados de los ciclos escolares.',
                            type: 'error'
                        });
                    })
                ];

            } else if (colectionName == 'programas') {

                promises = [$http({
                        method: 'GET',
                        url: __env.apiUrl + '/public/listados/departamentos'
                    }).then(function successCallback(response) {

                        console.log("respuesta ", response);
                        that.departamentos = response.data.result;
                        that.departamentoPrograma = that.buscaElemento(that.modelo.id_departamento, 'id_departamento', that.departamentos);

                    }, function errorCallback(response) {
                        PNotify.notify({
                            title: 'Error.',
                            text: 'Hubo un error al recuperar los listados de los departamentos.',
                            type: 'error'
                        });
                    })
                ];

                if(idRecurso){
                    promises.push($http({
                            method: 'GET',
                            url: __env.apiUrl + '/programa/' + that.modelo.id_programa + '/materias'
                        }).then(function successCallback(response) {

                            console.log("respuesta ", response);
                            that.modelo.elementosElegidos = response.data.result;
                            that.modelo.elementoElegido = null;

                        }, function errorCallback(response) {
                            PNotify.notify({
                                title: 'Error.',
                                text: 'Hubo un error al recuperar los listados de  materias por programa.',
                                type: 'error'
                            });
                        }));
                }

                return promises;

            } else if (colectionName == 'departamentos') {

                return [$http({
                    method: 'GET',
                    url: __env.apiUrl + '/public/listados/divisiones'
                }).then(function successCallback(response) {

                    console.log("respuesta ", response);
                    that.divisiones = response.data.result;
                    that.divisionDepartamento = that.buscaElemento(that.modelo.id_division, 'id_division', that.divisiones);

                }, function errorCallback(response) {
                    PNotify.notify({
                        title: 'Error.',
                        text: 'Hubo un error al recuperar los listados de los divisiones.',
                        type: 'error'
                    });
                })];

            } else if (colectionName == 'materias') {

                promises = [$http({
                        method: 'GET',
                        url: __env.apiUrl + '/public/listados/carreras'
                    }).then(function successCallback(response) {

                        console.log("respuesta ", '/public/listados/carreras', response);
                        that.programas = response.data.result;

                    }, function errorCallback(response) {
                        PNotify.notify({
                            title: 'Error.',
                            text: 'Hubo un error al recuperar los listados de las carreras.',
                            type: 'error'
                        });
                    })
                ];

                if(idRecurso){
                    promises.push($http({
                        method: 'GET',
                        url: __env.apiUrl + '/reportes/programas-x-materia/' + that.modelo.id_materia
                    }).then(function successCallback(response) {

                        console.log("respuesta ", '/reportes/programas-x-materia/' + that.modelo.id_materia, response);
                        that.modelo.elementosElegidos = response.data.result;
                        that.modelo.elementoElegido = null;
                        that.modelo.idsElegidos = _.map(that.elementosElegidos, function(m) {
                            return m.id_programa;
                        });


                    }, function errorCallback(response) {
                        PNotify.notify({
                            title: 'Error.',
                            text: 'Hubo un error al recuperar los listados de las carreras.',
                            type: 'error'
                        });
                    }));
                }

                return promises;

            } else {
                return [];
            }

        }

        that.closeWindow = function() {

            window.close();

        };

        that.getNombreCampoId = function(row) {

            // obtener el nombre del campo id
            return /id_\w+/.exec(Object.keys(row).join(" "))[0];

        };

        that.renderChip = function(data) {

            var nombreCampoId = that.getNombreCampoId(data);
            var idToDelete = data[nombreCampoId];
            var query = {};
            query[nombreCampoId] = data[nombreCampoId];

            if (!_.findWhere(that.modelo.elementosElegidos, query)) {
                that.modelo.idsElegidos.push(data[nombreCampoId]);

                that.modelo.idsRemovidos = _.reject(that.modelo.idsRemovidos, function(el) {
                    return el == idToDelete;
                });

                return data;
            }

        };

        that.removeChip = function(data) {
            var nombreCampoId = that.getNombreCampoId(data);
            var idToDelete = data[nombreCampoId];
            that.modelo.idsRemovidos.push(idToDelete);
            that.modelo.idsElegidos = _.reject(that.modelo.idsElegidos, function(el) {
                return el == idToDelete;
            });
            that.modelo.idsRemovidos = _.uniq(that.modelo.idsRemovidos);
            that.__onRemoveChip ? that.__onRemoveChip(data) : null;
            return true;
        };

        that.cargarModelo = function() {

            if(idRecurso){
                $http(that.colecciones[that.nombreColeccion].requests.fetch).then(function successCallback(response) {

                    that.modelo = response.data.result[0];
                    that.modelo.idsElegidos = [];
                    that.modelo.idsRemovidos = [];

                    $q.all(getPromises(that.nombreColeccion)).then(function() {

                        getMethods(that.nombreColeccion);

                    });

                }, function errorCallback(response) {
                    PNotify.notify({
                        title: 'Error.',
                        text: 'Hubo un error al recuperar el recurso.',
                        type: 'error'
                    });
                });
            }
            else{

                $q.all(getPromises(that.nombreColeccion)).then(function() {

                    getMethods(that.nombreColeccion);

                });

            }

        };

        that.guardarModelo = function() {

            var popUpText = 'El recurso ha sido creado.';
            var action = 'create';

            if(idRecurso){

                popUpText = 'El recurso ha sido modificado.';
                action = 'update';

            }

            var reqConf = that.colecciones[that.nombreColeccion].requests[action];
            reqConf.data = that.__formateaModelo ? that.__formateaModelo() : that.modelo;

            $http(reqConf).then(function successCallback(response) {

                var debugInfo = response.data.debugInfo;
                if(debugInfo.length){
                    popUpText += '\n\n'+debugInfo;
                }

                PNotify.notify({
                    title: 'Recurso modificado.',
                    text: popUpText,
                    type: 'success'
                });

                if(action == "create"){
                    that.inhabilitarGuardado = true;
                }

            }, function errorCallback(response) {

                var debugText = _.reduce(response.data.debugData, function(memo, dd) {

                    return memo + dd + '\n\n';

                }, "\n" + response.data.debugInfo + "\n\n");

                PNotify.notify({
                    title: 'Error.',
                    text: 'Hubo un error al modificar el recurso.' + debugText,
                    type: 'error'
                });
            });

        };

        that.guardarRecurso = function() {

            var conf = {
                title: "Se necesita confirmar la acción.",
                text: "¿Realmente desea crear/modificar el recurso?",
                yesCallback: function() {

                    that.guardarModelo();

                },
                noCallback: function() {



                }
            };

            PNotify.confirm(conf);

        }

        that.cargarModelo();

    }

})();
