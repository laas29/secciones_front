(function() {
    'use strict';
    angular.module('secciones_front').
    controller('LoginController', ['PNotify', '$scope', '$http', '$state', 'ACLService', LoginController]);

    function LoginController(PNotify, $scope, $http, $state, ACLService) {

        var that = this;

        that.codePattern = "^\\d{9,11}\\w?$";

        that.login = function() {

            PNotify.notify({
                    title: 'Cargando.',
                    text: 'Espere un momento mientras inicia su sesion.'
                });

            $http({
                method: 'POST',
                url: __env.apiUrl + '/public/alumno/login',
                data: that.inputs
            }).then(function successCallback(response) {

                console.log("respuesta ", response);
                that.programas = response.data.result;
                ACLService.setSessionValue('jwt_token', response.data.result[0].token);
                ACLService.reloadSessionValues();
                ACLService.goToDefaultSection();

                PNotify.notify({
                    title: 'Bienvenido.',
                    text: 'Has iniciado sesion.',
                    type: 'success'
                });

            }, function errorCallback(response) {
                PNotify.notify({
                    title: 'Error.',
                    text: response.data.debugInfo,
                    type: 'error'
                });
            });

        };

        that.userLogin = function() {

            PNotify.notify({
                    title: 'Cargando.',
                    text: 'Espere un momento mientras inicia su sesion.'
                });

            $http({
                method: 'POST',
                url: __env.apiUrl + '/public/usuario/login',
                data: that.inputs
            }).then(function successCallback(response) {

                console.log("respuesta ", response);
                that.programas = response.data.result;
                ACLService.setSessionValue('jwt_token', response.data.result[0].token);
                ACLService.reloadSessionValues();
                ACLService.goToDefaultSection();

                PNotify.notify({
                    title: 'Bienvenido.',
                    text: 'Has iniciado sesion.',
                    type: 'success'
                });

            }, function errorCallback(response) {
                PNotify.notify({
                    title: 'Error.',
                    text: response.data.debugInfo,
                    type: 'error'
                });
            });

        };

    }

})();
