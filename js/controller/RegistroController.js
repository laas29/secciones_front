(function() {
    'use strict';
    angular.module('secciones_front').
    controller('RegistroController', ['$http', '$scope', 'PNotify', '$state', RegistroController]);

    function RegistroController($http, $scope, PNotify, $state) {

        var that = this;
        that.inputs = {};

        // https://regex101.com/r/TEK80n/4
        // that.pswPattern = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=*.\\-_ \(\)\[\]\{\}\¡\!\?\¿\"\'\|])([a-zA-Z0-9@#$%^&+=*.\\-_ \(\)\[\]\{\}\¡\!\?\¿\"\'\|]){4,}$/;
        that.pswPattern = /^.{6,32}$/;
        that.codePattern = "^\\d{9,11}\\w?$";

        PNotify.notify({
            title: 'Bienvenido al sistema de preregistro.',
            text: 'Por favor llene la forma y enviela, una vez dado de alta podra elegir las materias del siguiente ciclo escolar.'
        });

        $http({
            method: 'GET',
            url: __env.apiUrl + '/public/listados/carreras'
        }).then(function successCallback(response) {

            console.log("respuesta ", response);
            that.programas = response.data.result;

        }, function errorCallback(response) {
            PNotify.notify({
                title: 'Error.',
                text: 'Hubo un error al recuperar los listados de las carreras.',
                type: 'error'
            });
        });

        $http({
            method: 'GET',
            url: __env.apiUrl + '/public/listados/ciclos'
        }).then(function successCallback(response) {

            console.log("ciclos ", response);
            that.ciclos = response.data.result;

        }, function errorCallback(response) {
            PNotify.notify({
                title: 'Error.',
                text: 'Hubo un error al recuperar los listados de los ciclos escolares.',
                type: 'error'
            });
        });

        that.revisaExistenciaAlumno = function() {

            console.log("Revisando existencia alumno");

            if( $scope.formRegistro.codigo.$valid ){

                $http({
                    method: 'GET',
                    url: __env.apiUrl + '/public/consulta/alumno/' + that.inputs.codigo
                }).then(function successCallback(response) {

                    if (response.data.result.length) {
                        console.log("El alumno ya existe");
                        $scope.formRegistro.codigo.registrado = true;
                    } else {
                        console.log("El alumno no existe");
                        $scope.formRegistro.codigo.registrado = false;
                    }


                }, function errorCallback(response) {
                    PNotify.notify({
                        title: 'Error.',
                        text: 'Hubo un error al buscar el código de estudiante.',
                        type: 'error'
                    });
                });

            }
            else{
                console.log("El codigo aun no es válido.");
            }

        }

        that.revisaContrasena = function() {

            if (that.inputs.contrasena == that.inputs.recontrasena) {
                $scope.formRegistro.recontrasena.noCoincide = false;
            } else {
                $scope.formRegistro.recontrasena.noCoincide = true;
            }

        };

        that.guardarAlumno = function() {

            var inputsCopy = angular.copy(that.inputs);

            inputsCopy.id_programa = that.inputs.carrera.id_programa;
            inputsCopy.id_ciclo = that.inputs.generacion.id_ciclo;
            delete inputsCopy.carrera;
            delete inputsCopy.generacion;

            $http({
                method: 'POST',
                url: __env.apiUrl + '/public/crear/alumno',
                data: inputsCopy
            }).then(function successCallback(response) {

                console.log("ciclos ", response);
                that.ciclos = response.data;

                PNotify.notify({
                    title: 'Registro exitoso.',
                    text: 'Se ha registrado exitosamente. Por favor proceda a iniciar sesión.',
                    type: 'success'
                });

                $state.go('login');

            }, function errorCallback(response) {

                var todosLosErrores = _.reduce(response.data.debugData, function(reduced, next) {
                    return reduced + '\n' + next;
                }, '');

                var todosLosErrores = response.data.debugInfo + '\n' + todosLosErrores;

                PNotify.notify({
                    title: 'Error.',
                    text: 'Hubo un error al guardar la información del alumno.\n\n' + todosLosErrores,
                    type: 'error'
                });
            });

        }

    }

})();
