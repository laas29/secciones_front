(function() {
    'use strict';
    angular.module('secciones_front').
    controller('DashboardController', ['ACLService', '$http', '$scope', 'PNotify', '$state',  DashboardController]);

    function DashboardController(ACLService, $http, $scope, PNotify, $state) {

        var that = this;
        that.inputs = {};
        that.preregistroModificado = false;

        $scope.$watch(
            function(){
                return $state.$current.navClasses;
            },
            function(){
                that.navClasses = $state.$current.navClasses;
            });

        that.datosUsuario = ACLService.getSessionValues();

        that.cerrarSesion = function (){

            if(that.preregistroModificado){
                PNotify.confirm({
                    title: 'No se han guardado los cambios.',
                    text: 'Aun no se han guardado los cambios en preregistro, ¿Seguro que deseas salir?',
                    yesCallback: function() {
                        that.preregistroModificado = false;
                        ACLService.logOut();
                        location.hash = "#/login";
                    },
                    noCallback: function() {

                    }
                });
            }
            else{
                ACLService.logOut();
                location.hash = "#/login";
            }
            
        }

    }

})();
