(function() {
    'use strict';
    angular.module('secciones_front').
    controller('MisDatosController', ['$http', '$scope', 'PNotify', '$state', 'ACLService', 'jwtHelper', MisDatosController]);

    function MisDatosController($http, $scope, PNotify, $state, ACLService, jwtHelper) {

        var that = this;
        that.inputs = {};
        that.contrasenas = {};
        that.camposContrasenaVacios = true;
        that.forms = {};
        that.contrasena = null;
        that.passwordInputsType = 'password';

        // https://regex101.com/r/TEK80n/4
        // that.pswPattern = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=*.\\-_ \(\)\[\]\{\}\¡\!\?\¿\"\'\|])([a-zA-Z0-9@#$%^&+=*.\\-_ \(\)\[\]\{\}\¡\!\?\¿\"\'\|]){4,}$/;
        that.pswPattern = /^.{6,32}$/;
        that.codePattern = "^\\d{9,11}\\w?$";
        that.emailPattern = /^[a-zA-Z0-9\.]+@[a-zA-Z0-9]+(\-)?[a-zA-Z0-9]+(\.)?[a-zA-Z0-9]{2,6}?\.[a-zA-Z]{2,6}$/;
        that.sessionValues = ACLService.getSessionValues();

        that.getCiclos = function() {

            return $http({
                url: __env.apiUrl + '/public/listados/ciclos', method: 'GET'
            }).then(function successCallback(response) {

                console.log("ciclos ", response);
                that.ciclos = response.data.result;

            }, function errorCallback(response) {
                PNotify.notify({
                    title: 'Error.',
                    text: 'Hubo un error al recuperar los listados de los ciclos escolares.',
                    type: 'error'
                });
            });

        };

        that.getCarreras = function() {

            return $http({
                method: 'GET',
                url: __env.apiUrl + '/public/listados/carreras'
            }).then(function successCallback(response) {

                console.log("respuesta ", response);
                that.programas = response.data.result;

            }, function errorCallback(response) {
                PNotify.notify({
                    title: 'Error.',
                    text: 'Hubo un error al recuperar los listados de las carreras.',
                    type: 'error'
                });
            });

        };

        that.getAlumno = function() {

            $http({
                url: __env.apiUrl + '/alumno/' + that.sessionValues.codigo,
                method: 'GET'
            }).then(function successCallback(response) {

                that.inputs = response.data.result[0];

                that.inputs.carrera = _.find(that.programas, function(el) {

                    if (el.id_programa == that.inputs.id_programa) {
                        return el;
                    }

                });

                that.inputs.generacion = _.find(that.ciclos, function(el) {

                    if (el.id_ciclo == that.inputs.id_generacion) {
                        return el;
                    }

                });

            }, function errorCallback(response) {
                PNotify.notify({
                    title: 'Error.',
                    text: 'Hubo un error al recuperar los datos del alumno.\n\n'+response.data.debugInfo,
                    type: 'error'
                });
            });

        };

        Promise.all([that.getCiclos(), that.getCarreras()]).then(function() {

                that.getAlumno();

            },
            function() {

                PNotify.notify({
                    title: 'Error.',
                    text: 'Hubo un error al recuperar los datos del alumno.',
                    type: 'error'
                });

            });

        that.revisaExistenciaAlumno = function() {

            console.log("Revisando existencia alumno");

            if (that.forms.formRegistro.codigo.$valid) {

                if(that.sessionValues.codigo != that.inputs.codigo){

                    $http({
                        method: 'GET',
                        url: __env.apiUrl + '/public/consulta/alumno/' + that.inputs.codigo
                    }).then(function successCallback(response) {

                        if (response.data.result.length) {
                            console.log("El alumno ya existe");
                            that.forms.formRegistro.codigo.registrado = true;
                        } else {
                            console.log("El alumno no existe");
                            that.forms.formRegistro.codigo.registrado = false;
                        }


                    }, function errorCallback(response) {
                        PNotify.notify({
                            title: 'Error.',
                            text: 'Hubo un error al buscar el código de estudiante.',
                            type: 'error'
                        });
                    });

                }


            } else {
                console.log("El codigo aun no es válido.");
            }

        }

        that.cambiarContrasena = function(){

            $http({
                method: 'PUT',
                url: __env.apiUrl + '/alumno/' + that.sessionValues.idAlumno + '/cambia-contrasena',
                data: that.contrasenas
            }).then(function successCallback(response) {

                PNotify.notify({
                    title: 'Contraseña modificada exitosamente.',
                    text: 'Tu contraseña se ha modificado exitosamente.',
                    type: 'success'
                });

            }, function errorCallback(response) {

                var todosLosErrores = _.reduce(response.data.debugData, function(reduced, next) {
                    return reduced + '\n' + next;
                }, '');

                var todosLosErrores = response.data.debugInfo + '\n' + todosLosErrores;

                PNotify.notify({
                    title: 'Error.',
                    text: 'Hubo un error al modificar tu contraseña.\n\n' + todosLosErrores,
                    type: 'error'
                });
            });

        };

        that.guardarAlumno = function() {

            var inputsCopy = angular.copy(that.inputs);

            inputsCopy.id_programa = that.inputs.carrera.id_programa;
            inputsCopy.id_ciclo = that.inputs.generacion.id_ciclo;
            delete inputsCopy.carrera;
            delete inputsCopy.generacion;

            $http({
                method: 'PUT',
                url: __env.apiUrl + '/alumno/' + that.sessionValues.idAlumno,
                data: inputsCopy
            }).then(function successCallback(response) {

                var token = response.data.result[0].token;
                var tokenPayload = jwtHelper.decodeToken(token);
                ACLService.setSessionValue('jwt_token', token)
                ACLService.setSessionValue('email', tokenPayload.email);
                ACLService.setSessionValue('carrera', tokenPayload.carrera);
                ACLService.setSessionValue('codigo', tokenPayload.codigo);
                ACLService.setSessionValue('nombre', tokenPayload.nombre);
                ACLService.setSessionValue('apellidos', tokenPayload.apellidos);
                ACLService.setSessionValue('idAlumno', tokenPayload.idAlumno);
                ACLService.setSessionValue('cicloActivo', tokenPayload.cicloActivo);

                // DashboardController
                $scope.dc.datosUsuario = tokenPayload;
                ACLService.reloadLocalStorage();

                PNotify.notify({
                    title: 'Datos modificados exitosamente.',
                    text: 'Tus datos personales se han modificado exitosamente.',
                    type: 'success'
                });

            }, function errorCallback(response) {

                var todosLosErrores = _.reduce(response.data.debugData, function(reduced, next) {
                    return reduced + '\n' + next;
                }, '');

                var todosLosErrores = response.data.debugInfo + '\n' + todosLosErrores;

                PNotify.notify({
                    title: 'Error.',
                    text: 'Hubo un error al guardar la información del alumno.\n\n' + todosLosErrores,
                    type: 'error'
                });
            });

        };

        that.muestraContrasenas = function(){

            that.passwordInputsType = that.passwordInputsType=='password'?'text':'password';

        };

        that.revisaContrasena = function() {


            if (that.contrasenas.nueva_contrasena == that.contrasenas.nueva_recontrasena) {
                that.forms.formContrasena.nueva_recontrasena.noCoincide = false;
            } else {
                that.forms.formContrasena.nueva_recontrasena.noCoincide = true;
            }

        };

    }

})();
