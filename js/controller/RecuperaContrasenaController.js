(function() {
    'use strict';
    angular.module('secciones_front').
    controller('RecuperaContrasenaController', ['PNotify', '$state', '$scope', '$http', RecuperaContrasenaController]);

    function RecuperaContrasenaController(PNotify, $state, $scope, $http) {

        var that = this;

        that.codePattern = "^\\d{9,11}\\w?$";

        that.scope = $scope;

        that.mostrarBotonEnvio = true;

        that.enviaCorreo = function(){

            if($scope.formRecuperaContrasena.$valid){

                that.mostrarBotonEnvio = false;
                that.inputs.codigo = that.inputs.codigo.trim();

                PNotify.notify({
                        title: 'Validando.',
                        text: 'Validando código de alumno.',
                        type: 'info'
                    });

                $http({
                    method: 'POST',
                    url: __env.apiUrl + '/public/alumno/recuperar-password',
                    data: that.inputs
                }).then(function successCallback(response) {

                    PNotify.notify({
                        title: 'Se te ha enviado un correo.',
                        text: 'Se ha enviado un email a tu dirección de correo con una nueva contraseña.',
                        type: 'success'
                    });

                    that.mostrarBotonEnvio = true;
                    $state.go('login');

                }, function errorCallback(error) {
                    PNotify.notify({
                        title: 'Error.',
                        text: 'El código proporcionado no está en el sistema.',
                        type: 'error'
                    });
                    that.mostrarBotonEnvio = true;
                });

            }
            else{

                PNotify.notify({
                        title: 'Especifica codigo.',
                        text: 'Por favor escribe tu codigo para poder recuperar el acceso a tu cuenta.'
                    });

            }

        };

    }

})();
