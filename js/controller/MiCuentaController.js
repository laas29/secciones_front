(function() {
    'use strict';
    angular.module('secciones_front').
    controller('MiCuentaController', ['$http', '$scope', 'PNotify', '$state', 'ACLService', 'jwtHelper', MiCuentaController]);

    function MiCuentaController($http, $scope, PNotify, $state, ACLService, jwtHelper) {

        var that = this;
        that.inputs = {};
        that.contrasenas = {};
        that.camposContrasenaVacios = true;
        that.forms = {};
        that.contrasena = null;
        that.passwordInputsType = 'password';

        // https://regex101.com/r/TEK80n/4
        // that.pswPattern = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=*.\\-_ \(\)\[\]\{\}\¡\!\?\¿\"\'\|])([a-zA-Z0-9@#$%^&+=*.\\-_ \(\)\[\]\{\}\¡\!\?\¿\"\'\|]){4,}$/;
        that.pswPattern = /^.{6,32}$/;
        that.codePattern = "^\\d{9,11}\\w?$";
        that.emailPattern = /^[a-zA-Z0-9\.]+@[a-zA-Z0-9]+(\-)?[a-zA-Z0-9]+(\.)?[a-zA-Z0-9]{2,6}?\.[a-zA-Z]{2,6}$/;
        that.sessionValues = ACLService.getSessionValues();

        that.getUser = function() {

            $http({
                url: __env.apiUrl + '/usuario/' + that.sessionValues.email,
                method: 'GET'
            }).then(function successCallback(response) {

                that.inputs = response.data.result[0];

            }, function errorCallback(response) {
                PNotify.notify({
                    title: 'Error.',
                    text: 'Hubo un error al recuperar los datos del usuario.\n\n'+response.data.debugInfo,
                    type: 'error'
                });
            });

        };

        that.revisaExistenciaUsuario = function() {

            if (that.forms.formRegistro.email.$valid) {

                if(that.sessionValues.email != that.inputs.email){

                    $http({
                        method: 'GET',
                        url: __env.apiUrl + '/usuario/' + that.inputs.email
                    }).then(function successCallback(response) {

                        if (response.data.result.length) {
                            console.log("El usuario ya existe");
                            that.forms.formRegistro.email.registrado = true;
                        } else {
                            console.log("El usuario no existe");
                            that.forms.formRegistro.email.registrado = false;
                        }


                    }, function errorCallback(response) {
                        PNotify.notify({
                            title: 'Error.',
                            text: 'Hubo un error al buscar el usuario.',
                            type: 'error'
                        });
                    });

                }


            } else {
                console.log("El email aun no es válido.");
            }

        }

        that.cambiarContrasena = function(){

            $http({
                method: 'PUT',
                url: __env.apiUrl + '/usuario/' + that.sessionValues.id_usuario + '/cambia-contrasena',
                data: that.contrasenas
            }).then(function successCallback(response) {

                PNotify.notify({
                    title: 'Contraseña modificada exitosamente.',
                    text: 'Tu contraseña se ha modificado exitosamente.',
                    type: 'success'
                });

            }, function errorCallback(response) {

                var todosLosErrores = _.reduce(response.data.debugData, function(reduced, next) {
                    return reduced + '\n' + next;
                }, '');

                var todosLosErrores = response.data.debugInfo + '\n' + todosLosErrores;

                PNotify.notify({
                    title: 'Error.',
                    text: 'Hubo un error al modificar tu contraseña.\n\n' + todosLosErrores,
                    type: 'error'
                });
            });

        };

        that.guardarUsuario = function() {

            var inputsCopy = angular.copy(that.inputs);

            $http({
                method: 'PUT',
                url: __env.apiUrl + '/usuario/' + that.sessionValues.id_usuario,
                data: inputsCopy
            }).then(function successCallback(response) {

                var token = response.data.result[0].token;
                var tokenPayload = jwtHelper.decodeToken(token);
                ACLService.setSessionValue('jwt_token', token)
                ACLService.setSessionValue('email', tokenPayload.email);
                ACLService.setSessionValue('puesto', tokenPayload.carrera);
                ACLService.setSessionValue('nombre', tokenPayload.nombre);
                ACLService.setSessionValue('apellidos', tokenPayload.apellidos);
                ACLService.setSessionValue('id_usuario', tokenPayload.id_usuario);

                // DashboardController
                $scope.dc.datosUsuario = tokenPayload;

                PNotify.notify({
                    title: 'Datos modificados exitosamente.',
                    text: 'Tus datos personales se han modificado exitosamente.',
                    type: 'success'
                });

            }, function errorCallback(response) {

                var todosLosErrores = _.reduce(response.data.debugData, function(reduced, next) {
                    return reduced + '\n' + next;
                }, '');

                var todosLosErrores = response.data.debugInfo + '\n' + todosLosErrores;

                PNotify.notify({
                    title: 'Error.',
                    text: 'Hubo un error al guardar la información del usuario.\n\n' + todosLosErrores,
                    type: 'error'
                });
            });

        };

        that.muestraContrasenas = function(){

            that.passwordInputsType = that.passwordInputsType=='password'?'text':'password';

        };

        that.revisaContrasena = function() {


            if (that.contrasenas.nueva_contrasena == that.contrasenas.nueva_recontrasena) {
                that.forms.formContrasena.nueva_recontrasena.noCoincide = false;
            } else {
                that.forms.formContrasena.nueva_recontrasena.noCoincide = true;
            }

        };

        that.getUser();

    }

})();
