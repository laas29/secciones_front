(function() {
    'use strict';
    angular.module('secciones_front').
    controller('EdicionController', ['$stateParams', '$http', '$scope', 'PNotify', '$state', 'NgTableParams', '$filter', 'localStorageService', '$window', '$q', EdicionController]);

    function EdicionController($stateParams, $http, $scope, PNotify, $state, ngTableParams, $filter, localStorageService, $window, $q) {

        var that = this,
            windowCounter = 0,
            idRecurso = null;

        that.customMethods = {};
        that.generateCSVLink = function(){
            that.colecciones[that.coleccionSeleccionada].csv.generate();
            that.csvLink = that.colecciones[that.coleccionSeleccionada].csv.link();
        };
        that.userRoles = [{ id: 'admin', title: 'admin' }, { id: 'user', title: 'user' }];
        that.coleccionSeleccionada = "";
        that.colecciones = {
            alumnos: {
                tabla: null,
                model: [],
                csv: {},
                requests: {
                    fetch: {
                        method: 'POST',
                        url: __env.apiUrl + '/reportes/alumnos',
                        data: { skip: 0, limit: 10 }
                    },
                    delete: {
                        method: 'DELETE',
                        url: __env.apiUrl + '/recurso/alumno'
                    }
                }
            },
            programas: {
                tabla: null,
                model: [],
                csv: {},
                removeWarning: 'Solo se podrá borrar el programa en caso de no tener alumnos registrados.\n\n',
                requests: {
                    fetch: {
                        method: 'POST',
                        url: __env.apiUrl + '/reportes/programas',
                        data: { skip: 0, limit: 10 }
                    },
                    delete: {
                        method: 'DELETE',
                        url: __env.apiUrl + '/recurso/programa'
                    }
                }
            },
            materias: {
                tabla: null,
                model: [],
                csv: {},
                otherModels: {},
                otherFilters: {},
                removeWarning: 'La materia solo podrá ser borrada si no está solicitada por algún preregistro.\n\n',
                requests: {
                    fetch: {
                        method: 'POST',
                        url: __env.apiUrl + '/reportes/materias',
                        data: { skip: 0, limit: 10 }
                        //data: {carreras: that.idsCarrerasElegidas, id_ciclo: that.ciclo.id_ciclo}
                    },
                    delete: {
                        method: 'DELETE',
                        url: __env.apiUrl + '/recurso/materia'
                    }
                }
            },
            ciclos: {
                tabla: null,
                model: [],
                csv: {},
                removeWarning: 'Solo se podrán borrar los ciclos que no hayan sido registrados como generación por algún estudiante o que no haya sido usado como ciclo de preregistro. Recuerde dejar suficientes ciclos para que los nuevos alumnos puedan elegir su generación.\n\n',
                requests: {
                    fetch: {
                        method: 'POST',
                        url: __env.apiUrl + '/reportes/ciclos',
                        data: { skip: 0, limit: 10 }
                    },
                    delete: {
                        method: 'DELETE',
                        url: __env.apiUrl + '/recurso/ciclo'
                    }
                }
            },
            centros: {
                tabla: null,
                model: [],
                csv: {},
                removeWarning: 'Se borrará el centro y los registros de movilidad relacionados.\n\n',
                requests: {
                    fetch: {
                        method: 'POST',
                        url: __env.apiUrl + '/reportes/centros',
                        data: { skip: 0, limit: 10 }
                    },
                    delete: {
                        method: 'DELETE',
                        url: __env.apiUrl + '/recurso/centro'
                    }
                }
            },
            departamentos: {
                tabla: null,
                model: [],
                csv: {},
                removeWarning: 'El recurso será borrado si no tiene dependencias.\n\n',
                requests: {
                    fetch: {
                        method: 'POST',
                        url: __env.apiUrl + '/reportes/departamentos',
                        data: { skip: 0, limit: 10 }
                        //data: {carreras: that.idsCarrerasElegidas, id_ciclo: that.ciclo.id_ciclo}
                    },
                    delete: {
                        method: 'GET',
                        url: __env.apiUrl + '/reportes/ciclo/' + $stateParams.idCiclo + '/materia/' + $stateParams.clave + '/programas/' + $stateParams.pathPrograms,
                        //data: {carreras: that.idsCarrerasElegidas, id_ciclo: that.ciclo.id_ciclo}
                    }
                }
            },
            usuarios: {
                tabla: null,
                model: [],
                csv: {},
                requests: {
                    fetch: {
                        method: 'POST',
                        url: __env.apiUrl + '/reportes/usuarios',
                        data: { skip: 0, limit: 10 }
                        //data: {carreras: that.idsCarrerasElegidas, id_ciclo: that.ciclo.id_ciclo}
                    },
                    delete: {
                        method: 'DELETE',
                        url: __env.apiUrl + '/recurso/usuario'
                    }
                }
            },
            preregistros: {
                tabla: null,
                model: [],
                removeWarning: 'Solo se podrá borrar el preregistro en caso de no estar activo.\n\n',
                csv: {},
                requests: {
                    fetch: {
                        method: 'POST',
                        url: __env.apiUrl + '/reportes/preregistros',
                        data: { skip: 0, limit: 10 }
                    },
                    delete: {
                        method: 'DELETE',
                        url: __env.apiUrl + '/recurso/preregistro'
                    }
                }
            }
        };

        that.editarRecurso = function(id, coleccion) {

            $window.open(location.origin+location.pathname + '#/editar-recurso/' + coleccion + '/' + id, 'Editar recurso ' + (windowCounter++), 'width=600px, height=400px');

        };

        that.nuevoRecurso = function() {

            $window.open(location.origin+location.pathname + '#/editar-recurso/' + that.coleccionSeleccionada, 'Nuevo recurso ' + (windowCounter++), 'width=600px, height=400px');

        };

        function getPromises(colectionName) {

            /*Si mi coleccion necesita de alguna promesa adicional*/

            var promises = [];

            if (colectionName == 'materias') {

                promises = [$http({
                    method: 'GET',
                    url: __env.apiUrl + '/public/listados/carreras'
                }).then(function successCallback(response) {

                    that.colecciones[that.coleccionSeleccionada].otherModels.programas = response.data.result;

                }, function errorCallback(response) {
                    PNotify.notify({
                        title: 'Error.',
                        text: 'Hubo un error al recuperar los listados de las carreras.',
                        type: 'error'
                    });
                })];

            }

            return promises;

        }

        function getMethods(colectionName) {

            /*Si mi coleccion necesita metodos especiales para trabajar*/

            that.customMethods = {};

            if (colectionName == 'materias') {

                that.customMethods.seleccionarMateria = function($item, $model, $label, $event) {

                    if ($item) {
                        that.colecciones.materias.otherFilters.id_programa = $item.id_programa;
                        that.colecciones[that.coleccionSeleccionada].tabla.page(1);
                        that.colecciones.materias.tabla.reload();
                    }

                };

                that.customMethods.validarSeleccion = function() {

                    if (!that.colecciones.materias.otherModels.carreraElegida) {
                        delete that.colecciones.materias.otherFilters.id_programa;
                        that.colecciones[that.coleccionSeleccionada].tabla.page(1);
                        that.colecciones.materias.tabla.reload();
                    }

                }

            }

        }

        that.cambiarColeccion = function() {

            $q.all(getPromises(that.coleccionSeleccionada)).then(function() {

                getMethods(that.coleccionSeleccionada);

            });

            that.cargaTabla();
        };

        that.cargaTabla = function() {

            if (!that.colecciones[that.coleccionSeleccionada].tabla) {
                that.colecciones[that.coleccionSeleccionada].tabla = new ngTableParams({
                    page: 1,
                    count: 10
                }, {
                    counts: [10, 50, 100, 300, 500, 1000],
                    filterDelay: 0,
                    getData: function(params) {

                        var coleccion = that.colecciones[that.coleccionSeleccionada];

                        var paramsObj = angular.copy(params.parameters());

                        if (coleccion.otherFilters) {
                            paramsObj.filter = angular.extend(paramsObj.filter, coleccion.otherFilters);
                        }

                        coleccion.requests.fetch.data = paramsObj;

                        return $http(coleccion.requests.fetch).then(function successCallback(response) {

                            coleccion.model = response.data.result.rows;

                            params.total(response.data.result.count);

                            return coleccion.model;

                        }, function errorCallback(response) {
                            PNotify.notify({
                                title: 'Error.',
                                text: 'Hubo un error al recuperar los recursos solicitados.',
                                type: 'error'
                            });
                        });

                    }
                });
            }

        };

        that.getNombreCampoId = function(row) {

            // obtener el nombre del campo id
            return /id_\w+/.exec(Object.keys(row).join(" "))[0];

        };

        that.eliminarRecurso = function(recurso) {

            var coleccion = that.colecciones[that.coleccionSeleccionada];

            var conf = {
                title: "Se necesita confirmar la acción.",
                text:  (coleccion.removeWarning || "") + "¿Realmente desea eliminar el recurso \""+recurso.nombre+"\"?",
                yesCallback: function() {

                    var confObj = angular.copy(coleccion.requests.delete);

                    confObj.url = confObj.url + '/' + recurso[that.getNombreCampoId(recurso)];

                    $http(confObj).then(function successCallback(response) {

                        PNotify.notify({
                            title: 'Recurso eliminado.',
                            text: 'El recurso se elimino satisfactoriamente.',
                            type: 'success'
                        });

                        coleccion.tabla.reload();

                    }, function errorCallback(response) {
                        PNotify.notify({
                            title: 'Error.',
                            text: 'Hubo un error al eliminar el recurso.' + (response.data.debugInfo?'\n\n'+response.data.debugInfo:''),
                            type: 'error'
                        });
                    });

                },
                noCallback: function() {

                }
            };

            PNotify.confirm(conf);

        }

    }

})();
