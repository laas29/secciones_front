(function() {
    'use strict';
    angular.module('secciones_front').
    controller('ReportesController', ['$http', '$scope', 'PNotify', '$state', 'NgTableParams', '$filter', 'localStorageService', '$window', ReportesController]);

    function ReportesController($http, $scope, PNotify, $state, ngTableParams, $filter, localStorageService, $window) {

        var that = this, reporteAlumnosSolicitantes = [], reporteMateriasCampus = [], reporteMateriasMovilidad = [], windowCounter = 0;
        that.programas = [];
        that.todosLosCampus = null;
        that.reporteMateriasMovilidad = [];
        that.tiposHorario = [
            { id: "", title: "Sin filtro" },
            { id: "Matutino", title: "Matutino" },
            { id: "Vespertino", title: "Vespertino" },
            { id: "Sabatino", title: "Sabatino" },
            { id: "Cualquiera", title: "Cualquiera" }
        ];
        that.idsCarrerasElegidas = [];
        that.origin = location.origin;
        that.ciclo;

        that.seleccionarDepartamento = function($item, $model, $label, $event){

            $http({
                method: 'GET',
                url: __env.apiUrl + '/public/listados/departamento/'+$item.id_departamento+'/carreras'
            }).then(function successCallback(response) {

                that.carrerasElegidas = response.data.result;
                that.idsCarrerasElegidas = _.map(that.carrerasElegidas, function(el){
                    return el.id_programa;
                });
                that.ciclo = null;

            }, function errorCallback(response) {
                PNotify.notify({
                    title: 'Error.',
                    text: 'Hubo un error al recuperar los listados de los departamentos.',
                    type: 'error'
                });
            });

        };

        that.generaUrlVerAlumno = function(row){

            var carreras = that.idsCarrerasElegidas.join('/');

            return location.origin+location.pathname + '#/reportes/ciclo/'+that.ciclo.id_ciclo+'/materia/'+row.clave+'/programas/'+carreras;

        };

        that.verAlumnos = function(row){

            $window.open(that.generaUrlVerAlumno(row), 'Alumnos pre-registrados ' + (windowCounter++), 'width=1000px, height=600px');

        };

        that.verAlumnosMovilidad = function(row){

            var carreras = that.idsCarrerasElegidas.join('/');

            $window.open(location.origin+location.pathname + '#/reportes/movilidad/ciclo/'+that.ciclo.id_ciclo+'/materia/'+row.clave+'/centro/'+row.id_centro+'/programas/'+carreras, 'Alumnos pre-registrados ' + (windowCounter++), 'width=1000px, height=600px');

        };

        that.renderChip = function(data){

            console.log(data);
            if(_.indexOf(that.idsCarrerasElegidas, data.id_programa) == -1){
                that.idsCarrerasElegidas.push(data.id_programa);
                that.recargarReporte();
                return data;
            }
            return;
        };

        that.removeChip = function(data){

            console.log(data);
            var posicionAQuitar = _.indexOf(that.idsCarrerasElegidas, data.id_programa);
            if(posicionAQuitar != -1){
                that.idsCarrerasElegidas.splice(posicionAQuitar, 1);
                that.carrerasElegidas.splice(posicionAQuitar, 1);
                that.recargarReporte();
            }
            return true;

        }

        that.recargarReporte = function(){

            if(that.idsCarrerasElegidas.length && that.ciclo){

                $http({
                    method: 'POST',
                    url: __env.apiUrl + '/reportes/por-carreras',
                    data: {carreras: that.idsCarrerasElegidas, id_ciclo: that.ciclo.id_ciclo}
                }).then(function successCallback(response) {

                    reporteMateriasCampus = response.data.result.campus;
                    reporteMateriasMovilidad = response.data.result.movilidad;
                    that.ngTableMateriasCarrera.reload();
                    that.ngTableMateriasMovilidad.reload();

                }, function errorCallback(response) {
                    PNotify.notify({
                        title: 'Error.',
                        text: 'Hubo un error al recuperar los reportes por carreras.',
                        type: 'error'
                    });
                });

                $http({
                    method: 'POST',
                    url: __env.apiUrl + '/reportes/alumnos-solicitantes',
                    data: {carreras: that.idsCarrerasElegidas, id_ciclo: that.ciclo.id_ciclo}
                }).then(function successCallback(response) {

                    reporteAlumnosSolicitantes = response.data.result;
                    that.ngTableAlumnosSolicitantes.reload();

                }, function errorCallback(response) {
                    PNotify.notify({
                        title: 'Error.',
                        text: 'Hubo un error al recuperar los alumnos solicitantes.',
                        type: 'error'
                    });
                });

            }

        }

        $http({
            method: 'GET',
            url: __env.apiUrl + '/public/listados/carreras'
        }).then(function successCallback(response) {

            that.programas = response.data.result;

        }, function errorCallback(response) {
            PNotify.notify({
                title: 'Error.',
                text: 'Hubo un error al recuperar los listados de las carreras.',
                type: 'error'
            });
        });

        $http({
            method: 'GET',
            url: __env.apiUrl + '/public/listados/ciclos'
        }).then(function successCallback(response) {

            console.log("ciclos ", response);
            that.ciclos = response.data.result;

        }, function errorCallback(response) {
            PNotify.notify({
                title: 'Error.',
                text: 'Hubo un error al recuperar los listados de los ciclos escolares.',
                type: 'error'
            });
        });

        $http({
            method: 'GET',
            url: __env.apiUrl + '/public/listados/departamentos'
        }).then(function successCallback(response) {

            console.log("departamentos ", response);
            that.departamentos = response.data.result;

        }, function errorCallback(response) {
            PNotify.notify({
                title: 'Error.',
                text: 'Hubo un error al recuperar los listados de los departamentos.',
                type: 'error'
            });
        });

        that.ngTableMateriasMovilidad = new ngTableParams({
            page: 1,
            count: 10
        }, {
            counts: [10, 50, 100, 300, 500, 1000],
            filterDelay: 0,
            getData: function(params) {

                var inputs = params.filter();
                var sorting = params.sorting();

                var materiasFiltradas = reporteMateriasMovilidad;

                materiasFiltradas = params.sorting() ? $filter('orderBy')(materiasFiltradas, params.orderBy()) : materiasFiltradas;
                materiasFiltradas = params.filter() ? $filter('filter')(materiasFiltradas, params.filter()) : materiasFiltradas;
                params.total(materiasFiltradas.length);
                materiasFiltradas = materiasFiltradas.slice((params.page() - 1) * params.count(), params.page() * params.count());

                return materiasFiltradas;

            }
        });

        that.ngTableMateriasCarrera = new ngTableParams({
            page: 1,
            count: 10
        }, {
            counts: [10, 50, 100, 300, 500, 1000],
            filterDelay: 0,
            getData: function(params) {

                var inputs = params.filter();
                var sorting = params.sorting();

                var materiasFiltradas = reporteMateriasCampus;

                materiasFiltradas = params.sorting() ? $filter('orderBy')(materiasFiltradas, params.orderBy()) : materiasFiltradas;
                materiasFiltradas = params.filter() ? $filter('filter')(materiasFiltradas, params.filter()) : materiasFiltradas;
                params.total(materiasFiltradas.length);
                materiasFiltradas = materiasFiltradas.slice((params.page() - 1) * params.count(), params.page() * params.count());

                return materiasFiltradas;

            }
        });

        that.ngTableAlumnosSolicitantes = new ngTableParams({
            page: 1,
            count: 10
        }, {
            counts: [10, 50, 100, 300, 500, 1000],
            filterDelay: 0,
            getData: function(params) {

                var inputs = params.filter();
                var sorting = params.sorting();

                var alumnosSolicitantes = reporteAlumnosSolicitantes;

                alumnosSolicitantes = params.sorting() ? $filter('orderBy')(alumnosSolicitantes, params.orderBy()) : alumnosSolicitantes;
                alumnosSolicitantes = params.filter() ? $filter('filter')(alumnosSolicitantes, params.filter()) : alumnosSolicitantes;
                params.total(alumnosSolicitantes.length);
                alumnosSolicitantes = alumnosSolicitantes.slice((params.page() - 1) * params.count(), params.page() * params.count());

                return alumnosSolicitantes;

            }
        });

    }

})();
