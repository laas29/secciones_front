/*

    // Koala

    // libraries
    @koala-prepend "../bower_components/jquery/dist/jquery.min.js"
    @koala-prepend "../bower_components/moment/min/moment-with-locales.js"
    @koala-prepend "../bower_components/angular/angular.min.js"
    @koala-prepend "../bower_components/angular-ui-router/release/angular-ui-router.min.js"
    @koala-prepend "../bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"
    @koala-prepend "../bower_components/pnotify/dist/pnotify.js"
    @koala-prepend "../bower_components/pnotify/dist/pnotify.buttons.js"
    @koala-prepend "../bower_components/pnotify/dist/pnotify.confirm.js"
    @koala-prepend "../bower_components/pnotify/dist/pnotify.history.js"
    @koala-prepend "../bower_components/pnotify/dist/pnotify.callbacks.js"
    @koala-prepend "../bower_components/underscore/underscore-min.js"
    @koala-prepend "./lib/ng-table/ng-table.min.js"
    @koala-prepend "../bower_components/angular-local-storage/dist/angular-local-storage.min.js"
    @koala-prepend "../bower_components/angular-jwt/dist/angular-jwt.min.js"
    @koala-prepend "../bower_components/ng-table-to-csv/dist/ng-table-to-csv.min.js"
    @koala-prepend "../bower_components/angular-chips/dist/angular-chips.min.js"

    // services
    @koala-append "service/ACLService.js"
    @koala-append "service/PNotifyService.js"

    // controllers
    @koala-append "controller/SignController.js"
    @koala-append "controller/LoginController.js"
    @koala-append "controller/RegistroController.js"
    @koala-append "controller/DashboardController.js"
    @koala-append "controller/MisDatosController.js"
    @koala-append "controller/PreRegistroController.js"
    @koala-append "controller/RecuperaContrasenaController.js"
    @koala-append "controller/ReportesController.js"
    @koala-append "controller/ReportesStandAloneController.js"
    @koala-append "controller/MiCuentaController.js"
    @koala-append "controller/EdicionController.js"
    @koala-append "controller/EdicionRecursoController.js"

    // directives
    @koala-append "directive/disableOnRequest.js"

*/

// deshabilita los mensajes de depuracion
if(!window.__env.enableDebug){
    console = {log: function(){}, error: function(){}, warn: function(){}};
}

if(typeof String.prototype.trim !== 'function') {
  String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, '');
  }
}

$.ajaxPrefilter(function(options, original_Options, jqXHR) {
    options.async = true;
});

var app = angular.module('secciones_front', ['ui.router', 'ui.bootstrap', 'ngTable', 'LocalStorageModule', 'angular-jwt', 'ngTableToCsv', 'angular.chips']).run(['$state', '$rootScope', 'authManager', function($state, $rootScope, authManager) {
    $rootScope.$on("$stateChangeError", console.log.bind(console));

    $rootScope.$on('$stateChangeStart', onStateChangeHandler);

    function onStateChangeHandler(event, toState, toParams, fromState, fromParams, options) {

    };

    $rootScope.$on('tokenHasExpired', function() {
        alert(' sesion se ha expirado');
    });

    authManager.checkAuthOnRefresh();
    authManager.redirectWhenUnauthenticated();

}]).config(['jwtInterceptorProvider', '$stateProvider', '$urlRouterProvider', '$httpProvider', 'jwtOptionsProvider', 'localStorageServiceProvider', function(jwtInterceptorProvider, $stateProvider, $urlRouterProvider, $httpProvider, jwtOptionsProvider, localStorageServiceProvider) {

    localStorageServiceProvider.setPrefix('secciones_front');

    // Please note we're annotating the function so that the $injector works when the file is minified
    jwtOptionsProvider.config({
        // unauthenticatedRedirectPath: '#/login',
        unauthenticatedRedirector: ['$state', 'localStorageService', function($state, localStorageService) {
            var role = localStorageService.get('role');
            if (role && role != 'alumno') {
                $state.go('user-login');
            } else {
                $state.go('login');
            }
        }],
        tokenGetter: ['localStorageService', 'jwtHelper', 'ACLService', function(localStorageService, jwtHelper, ACLService) {

            var token = localStorageService.get('jwt_token'), role = null;

            if(token){
                if(jwtHelper.isTokenExpired(token)){
                    token = null;
                    localStorageService.clearAll();
                }
            }

            return token;

        }],
        whiteListedDomains: __env.whiteListedDomains
    });

    $httpProvider.interceptors.push('jwtInterceptor');

    $urlRouterProvider.otherwise('home');

    $stateProvider
        .state('index', {
            abstract: true,
            templateUrl: window.__env.baseUrl+'/index.html',
            authenticate: false
        })
        .state('sign', {
            parent: 'index',
            abstract: true,
            templateUrl: window.__env.baseUrl+'/html/sign.html',
            authenticate: false
        })
        .state('home', {
            parent: 'sign',
            url: '/home',
            authenticate: false,
            views: {
                'signContent@sign': { templateUrl: window.__env.baseUrl+'/html/seleccion.html' },
            }
        })
        .state('registro', {
            parent: 'sign',
            url: '/registro',
            authenticate: false,
            views: {
                'signContent@sign': { templateUrl: window.__env.baseUrl+'/html/registro.html' },
            }
        })
        .state('login', {
            parent: 'sign',
            url: '/login',
            authenticate: false,
            views: {
                'signContent@sign': { templateUrl: window.__env.baseUrl+'/html/login.html' },
            }
        })
        .state('user-login', {
            parent: 'sign',
            url: '/user-login',
            authenticate: false,
            views: {
                'signContent@sign': { templateUrl: window.__env.baseUrl+'/html/user-login.html' },
            }
        })
        .state('recupera-contrasena', {
            parent: 'sign',
            url: '/recupera-contrasena',
            authenticate: false,
            views: {
                'signContent@sign': { templateUrl: window.__env.baseUrl+'/html/recupera-contrasena.html' },
            }
        })
        .state('dashboard', {
            parent: 'index',
            abstract: true,
            templateUrl: window.__env.baseUrl+'/html/dashboard.html',
            authenticate: true,
            data: {
                requiresLogin: true
            }
        })
        .state('listado-alumnos', {
            parent: 'index',
            // /reportes/ciclo/1/materia/CC210/programas/34/91
            url: '/reportes/ciclo/:idCiclo/materia/:clave/programas/{pathPrograms:.*}',
            templateUrl: window.__env.baseUrl+'/html/listadoAlumnos.html',
            authenticate: true,
            data: {
                requiresLogin: true
            },
            resolve: {

                roleHasPermission: ['ACLService', function(ACLService) {

                    return ACLService.verify("listado-alumnos");

                }]

            }
        })
        .state('listado-alumnos.movilidad', {
            parent: 'index',
            // /reportes/ciclo/1/materia/CC210/programas/34/91
            url: '/reportes/movilidad/ciclo/:idCiclo/materia/:clave/centro/:id_centro/programas/{pathPrograms:.*}',
            templateUrl: window.__env.baseUrl+'/html/listadoAlumnos.html',
            authenticate: true,
            data: {
                requiresLogin: true
            },
            resolve: {

                roleHasPermission: ['ACLService', function(ACLService) {

                    return ACLService.verify("listado-alumnos");

                }]

            }
        })
        .state('editar-recurso', {
            parent: 'index',
            url: '/editar-recurso/{pathPrograms:.*}',
            templateUrl: window.__env.baseUrl+'/html/edicion-modal.html',
            authenticate: true,
            data: {
                requiresLogin: true
            },
            resolve: {

                roleHasPermission: ['ACLService', function(ACLService) {

                    return ACLService.verify("editar-recurso");

                }]

            }
        })
        .state('pre-registro', {
            parent: 'dashboard',
            url: '/pre-registro',
            authenticate: true,
            views: {
                'dashboardContent@dashboard': { templateUrl: window.__env.baseUrl+'/html/pre-registro.html' }
            },
            data: {
                requiresLogin: true
            },
            navClasses: {
                preRegistro: 'active'
            },
            resolve: {

                roleHasPermission: ['ACLService', function(ACLService) {

                    return ACLService.verify("pre-registro");

                }]

            }
        })
        .state('mis-datos', {
            parent: 'dashboard',
            url: '/mis-datos',
            authenticate: true,
            views: {
                'dashboardContent@dashboard': { templateUrl: window.__env.baseUrl+'/html/mis-datos.html' }
            },
            data: {
                requiresLogin: true
            },
            navClasses: {
                misDatos: 'active'
            },
            resolve: {

                roleHasPermission: ['ACLService', function(ACLService) {

                    return ACLService.verify("mis-datos");

                }]

            }
        })
        .state('edicion', {
            parent: 'dashboard',
            url: '/edicion',
            authenticate: true,
            views: {
                'dashboardContent@dashboard': { templateUrl: window.__env.baseUrl+'/html/edicion.html' }
            },
            data: {
                requiresLogin: true
            },
            navClasses: {
                edicion: 'active'
            },
            resolve: {

                roleHasPermission: ['ACLService', function(ACLService) {

                    return ACLService.verify("edicion");

                }]

            }
        })
        .state('mi-cuenta', {
            parent: 'dashboard',
            url: '/mi-cuenta',
            authenticate: true,
            views: {
                'dashboardContent@dashboard': { templateUrl: window.__env.baseUrl+'/html/mi-cuenta.html' }
            },
            data: {
                requiresLogin: true
            },
            navClasses: {
                miCuenta: 'active'
            },
            resolve: {

                roleHasPermission: ['ACLService', function(ACLService) {

                    return ACLService.verify("mi-cuenta");

                }]

            }
        })
        .state('reportes', {
            parent: 'dashboard',
            url: '/reportes',
            authenticate: true,
            views: {
                'dashboardContent@dashboard': { templateUrl: window.__env.baseUrl+'/html/reportes.html' }
            },
            data: {
                requiresLogin: true
            },
            navClasses: {
                reportes: 'active'
            },
            resolve: {

                roleHasPermission: ['ACLService', function(ACLService) {

                    return ACLService.verify('reportes');

                }]

            }
        });

}]);
