(function() {
    'use strict';
    angular.module('secciones_front')
        .directive('disableonrequest', ['$http', disableonrequest])

    function disableonrequest($http) {
        return {
            link: function(scope, element, attrs) {
                scope.$watch(function() {
                    return $http.pendingRequests.length > 0;
                }, function(request) {
                    //console.log("attrs " + attrs);
                    //If the form is invalid disableonrequest is true, so if the form is invalid, we dont need to do anything.
                    var formInvalid = true;
                    //If undefined, lets assume is valid
                    if (attrs.disableonrequest === undefined) {
                        formInvalid = false;
                    } else {
                        formInvalid = attrs.disableonrequest === 'true' ? true : false;
                    }
                    if (!formInvalid) {
                        if (!request) {
                            element.prop('disabled', false);
                        } else {
                            element.prop('disabled', true);
                        }
                    }
                });
            }
        }
    }
});
