(function() {
    'use strict';
    app.factory('ACLService', ['localStorageService', 'jwtHelper', '$state', '$location', ACLService]);

    function ACLService(localStorageService, jwtHelper, $state, $location) {
        var that = this;
        var datosUsuario = null;
        var acl = {
            alumno: { default: 'pre-registro', sections: ['mis-datos', 'pre-registro'] },
            user: { default: 'reportes', sections: ['mi-cuenta', 'reportes', 'listado-alumnos'] },
            admin: { default: 'reportes', sections: ['mi-cuenta', 'reportes', 'listado-alumnos', 'edicion', 'editar-recurso'] }
        };
        that.currentPermissions = {};

        function setSessionValues() {
            var tokenPayload = localStorageService.get('jwt_token');
            if (tokenPayload) {
                tokenPayload = jwtHelper.decodeToken(tokenPayload);
                localStorageService.set('email', tokenPayload.email);
                localStorageService.set('nombre', tokenPayload.nombre);
                localStorageService.set('apellidos', tokenPayload.apellidos);
                localStorageService.set('role', tokenPayload.role);
                if (tokenPayload.role == 'alumno') {
                    localStorageService.set('carrera', tokenPayload.carrera);
                    localStorageService.set('codigo', tokenPayload.codigo);
                    localStorageService.set('idAlumno', tokenPayload.idAlumno);
                    localStorageService.set('cicloActivo', tokenPayload.cicloActivo);
                }
                datosUsuario = tokenPayload;
            }
        };

        that.setSessionValue = function(key, value){

            localStorageService.set(key, value);

        };

        that.reloadSessionValues = function(){
            setSessionValues();
        };

        that.reloadLocalStorage = function(){
            datosUsuario = jwtHelper.decodeToken(localStorageService.get('jwt_token'));
        };

        that.getSessionValues = function() {
            return datosUsuario;
        };

        that.verify = function(section) {

            return new Promise(function(resolve, reject){

                if (!datosUsuario) {
                    setSessionValues();
                }
                var currentPermissions = acl[localStorageService.get('role')];
                if ( !currentPermissions || _.indexOf(currentPermissions.sections, section) == -1) {
                    $location.path(currentPermissions.default);
                    reject();
                }
                else{
                    resolve();
                }

            });

        };

        that.logOut = function() {

            datosUsuario = null;
            localStorageService.clearAll();

        };

        that.goToDefaultSection = function() {
            $state.go(acl[localStorageService.get('role')].default);
        };

        return that;
    }

})();
