app.factory('PNotify', function() {
    'use strict';
    var that = this;
    // PNotify.prototype.options.styling = "bootstrap3";

    this.notify = function(conf) {

        PNotify.removeAll();

        var _notify = new PNotify({
            title: conf.title,
            text: conf.text,
            // type: "notice" - Type of the notice. "notice", "info", "success", or "error".
            type: conf.type || 'info',
            width: conf.width || "300px",
            overlay_close: conf.overlay_close || true,
            animation: conf.animation || 'fade',
            nonblock: {
                nonblock: true,
                sticker: false,
                closer: false
            }
        }).get().click(function() {
            _notify.remove();
        });

    }

    this.confirm = function(conf) {

        PNotify.removeAll();

        var _notify = new PNotify({
            title: conf.title,
            text: conf.text,
            icon: 'glyphicon glyphicon-question-sign',
            hide: false,
            confirm: {
                confirm: true
            },
            buttons: {
                closer: false,
                sticker: false
            },
            history: {
                history: false
            },
            addclass: 'stack-modal',
            stack: {
                'dir1': 'down',
                'dir2': 'right',
                'modal': true
            }
        }).get().on('pnotify.confirm', function() {
            conf.yesCallback();
        }).on('pnotify.cancel', function() {
            conf.noCallback();
        });

    }

    return this;

});
