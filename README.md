# Sistema para el manejo de los cupos por secciones #

### Integrantes ###

* Luis Arias laas29@gmail.com
* Maria Espinosa thedarkwoodscircus@gmail.com

### Instrucciones ###

Para poder hacer uso de la aplicacion correctamente, se necesita configurar las variables de en entorno "/js/env.js".
Junto con el codigo del proyecto se incluye un archivo de ejemplo (env.example.js) con cada una de las variables
de entorno requeridas para hacer funcionar la aplicación. 

Para poder compilar el js (minificarlo y unir los archivos y sus dependencias) necesita koala (http://koala-app.com/).

Bower es el manejador de dependencias del proyeccto con el puede cargar las librerias que le hacen falta (solo si no se 
incluyo carpeta bower_components y necesita volver a bajar las dependencias).